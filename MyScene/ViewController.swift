//
//  ViewController.swift
//  MyScene
//
//  Created by Anders Zetterström on 2015-04-04.
//  Copyright (c) 2015 Anders Zetterström. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var candies = [Candy]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.candies = [Candy(name: "Jolly Rancher"),
                         Candy(name: "Snickers"),
                         Candy(name: "Mars"),
                         Candy(name: "Butterfinger"),
                         Candy(name: "Twix")]
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.candies.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        var candy : Candy
        
        candy = candies[indexPath.row]
        
        cell.textLabel?.text = candy.name
        
        
        return cell
    }


}
































